import json
import requests
import lxml
from bs4 import BeautifulSoup


class FindWheel:
    def __init__(self):
        self.headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'
        }
        self.url = 'https://emex.ru/catalogs2/201'

    def wheels(self):
        response = requests.get(url=self.url, headers=self.headers)
        a = response.text
        test = BeautifulSoup(a, 'lxml')
        print(test)

        result = test.find_all('div', class_='c163jr1i')
        wheels_data = []
        for i in result:
            print(i)
            brand_name = i.find(class_='tkvps9j')
            size = i.find('div', class_='ty19mw6').find('div', class_='t17djcha')
            radius = i.find('div', class_='ty19mw6').find_all(class_='t17djcha')[1]
            wheels_data.append(
                {'title': 'Тапки для тачки Виталика',
                 'brand_name': brand_name.text,
                 'size': size.text,
                 'radius': radius.text
                 }
            )
            with open('test.json', 'a', encoding='utf-8') as file:
                json.dump(wheels_data, file, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    b = FindWheel()
    b.wheels()
